<?php
require_once('autoload.php');

require_once("header.php");

if(isset ($_GET['page']) && file_exists($_GET['page'].'.php')) {
  require_once($_GET['page'].'.php');
} elseif (!isset($_GET['page'])) {
  require_once("homepage.php");
} else {
  require_once("404.html");
}

require_once("footer.php");
