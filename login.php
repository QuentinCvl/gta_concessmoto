<div class="container-fluid globalMarginTop login">
  <div class="col-xl-12 col-md-12 mb-12 loginPopUp">
    <h2 class="mb-4">Connexion Administrateur</h2>
    <form method="post" action="index.php?page=login">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="login_username">Utilisateur</span>
        </div>
        <input type="text" class="form-control" placeholder="username" name="login_username">
      </div>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="login_pswd">MDP</span>
        </div>
        <input type="password" class="form-control" placeholder="mot de passe" name="login_pswd">
      </div>

      <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
             name="login-button">Valider
      </button>
    </form>
  </div>
  <?php
  if (isset($_POST['login-button'])) {
    $login = new Login($_POST['login_username'], $_POST['login_pswd']);
    $login->connexion();
  }
  ?>
</div>
