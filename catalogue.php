<!--::breadcrumb part start::-->
<section class="breadcrumb blog_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumb_iner">
          <div class="breadcrumb_iner_item">
            <h2> Catalogues </h2>
            <p>En cours de création</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--::breadcrumb part end::-->
<section class="catalogue section-padding about_part" id="withoutlicense">
  <div class="container-fluid">
    <div class="row">
      <div class="section_tittle">
        <h2><span>Sans Permis</span></h2>
      </div>
    </div>

    <div class="article_list" id="withoutlicense">

      <?php
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $req = $dbh->prepare('SELECT * FROM vehicles WHERE category = :cat ORDER BY price');
      $req->execute(array(':cat' => "withoutlicense"));

      if (!empty($req)) {
        foreach ($req as $row) {
          echo '<div class="card" style="width: 18rem; margin: 2%;">
            <img class="card-img-top" src="' . $row['src'] . '">
            <div class="card-body">
              <h5 class="card-title">' . $row['name'] . '  -  ' . $row['price'] . ' $' . '</h5> 
            </div>
          </div>';
        }
      }
      ?>
    </div>
  </div>
</section>
<section class="catalogue section-padding about_part" id="roadster">
  <div class="container-fluid">
    <div class="row">
      <div class="section_tittle">
        <h2><span>Roadster</span></h2>
      </div>
    </div>

    <div class="article_list" id="withoutlicense">

      <?php
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $req = $dbh->prepare('SELECT * FROM vehicles WHERE category = :cat ORDER BY price');
      $req->execute(array(':cat' => "roadster"));

      if (!empty($req)) {
        foreach ($req as $row) {
          echo '<div class="card" style="width: 18rem; margin: 2%;">
            <img class="card-img-top" src="' . $row['src'] . '">
            <div class="card-body">
              <h5 class="card-title">' . $row['name'] . '  -  ' . $row['price'] . ' $' . '</h5> 
            </div>
          </div>';
        }
      }
      ?>
    </div>
  </div>
</section>
<section class="catalogue section-padding about_part" id="chopper">
  <div class="container-fluid">
    <div class="row">
      <div class="section_tittle">
        <h2><span>Chopper</span></h2>
      </div>
    </div>

    <div class="article_list" id="withoutlicense">

      <?php
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $req = $dbh->prepare('SELECT * FROM vehicles WHERE category = :cat ORDER BY price');
      $req->execute(array(':cat' => "chopper"));

      if (!empty($req)) {
        foreach ($req as $row) {
          echo '<div class="card" style="width: 18rem; margin: 2%;">
            <img class="card-img-top" src="' . $row['src'] . '">
            <div class="card-body">
              <h5 class="card-title">' . $row['name'] . '  -  ' . $row['price'] . ' $' . '</h5> 
            </div>
          </div>';
        }
      }
      ?>
    </div>
  </div>
</section>
<section class="catalogue section-padding about_part" id="offroad">
  <div class="container-fluid">
    <div class="row">
      <div class="section_tittle">
        <h2><span>Off-Road</span></h2>
      </div>
    </div>

    <div class="article_list" id="withoutlicense">

      <?php
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $req = $dbh->prepare('SELECT * FROM vehicles WHERE category = :cat ORDER BY price');
      $req->execute(array(':cat' => "offroad"));

      if (!empty($req)) {
        foreach ($req as $row) {
          echo '<div class="card" style="width: 18rem; margin: 2%;">
            <img class="card-img-top" src="' . $row['src'] . '">
            <div class="card-body">
              <h5 class="card-title">' . $row['name'] . '  -  ' . $row['price'] . ' $' . '</h5> 
            </div>
          </div>';
        }
      }
      ?>
    </div>
  </div>
</section>
<section class="catalogue section-padding about_part" id="sportive">
  <div class="container-fluid">
    <div class="row">
      <div class="section_tittle">
        <h2><span>Sportive</span></h2>
      </div>
    </div>

    <div class="article_list" id="withoutlicense">

      <?php
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $req = $dbh->prepare('SELECT * FROM vehicles WHERE category = :cat ORDER BY price');
      $req->execute(array(':cat' => "sportive"));

      if (!empty($req)) {
        foreach ($req as $row) {
          echo '<div class="card" style="width: 18rem; margin: 2%;">
            <img class="card-img-top" src="' . $row['src'] . '">
            <div class="card-body">
              <h5 class="card-title">' . $row['name'] . '  -  ' . $row['price'] . ' $' . '</h5> 
            </div>
          </div>';
        }
      }
      ?>
    </div>
  </div>
</section>
