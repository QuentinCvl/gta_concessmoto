<?php
if (!isset($_SESSION['username'])) {
    echo '<script> document.location.replace("index.php?page=login");</script>';
}
?>
<section class="breadcrumb blog_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner">
                    <div class="breadcrumb_iner_item">
                        <h2> Administration </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--::breadcrumb part end::-->

<div style="text-align: center; margin-top: 2%">
    <a href="index.php?page=disconnect">
        <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm">
            Deconnexion
        </button>
    </a>
</div>
<?php
if(isset($_POST['update_vehicle_button']) && isset($_POST['update_vehicle_id'])) {
    if(isset($_FILES["update_vehicle_src"])) { // Si il y a une nouvelle image, on va supprimer l'ancienne
        $currentsrc = $_POST['curent_vehicle_src'];
        unlink($currentsrc);
        $target_dir = "data/vehicules/";
        $imageFileType = strtolower(pathinfo($_FILES["update_vehicle_src"]['name'], PATHINFO_EXTENSION));
        $target_file = $target_dir . $_POST['update_vehicle_name'] . '.' . $imageFileType;
        if (move_uploaded_file($_FILES["update_vehicle_src"]["tmp_name"], $target_file)) {
            $new_vehicle = new Vehicles($_POST['update_vehicle_name']);
            $new_vehicle->updateVehiculeWithImage($_POST['update_vehicle_id'], $_POST['update_vehicle_price'],
                $_POST['update_vehicle_cat'], $target_file);
        }
    } else {
        $new_vehicle = new Vehicles($_POST['update_vehicle_name']);
        $new_vehicle->updateVehicule($_POST['update_vehicle_id'], $_POST['update_vehicle_price'],
            $_POST['update_vehicle_cat']);
        }
}
?>
<section class="about_part section-padding" id="about">
    <div class="container">
        <div class="row">
            <div class="section_tittle">
                <h2>Création <span>de produit</span></h2>
            </div>
        </div>
        <?php
        if (isset($_POST['create-vehicle-button'])) {
            $target_dir = "data/vehicules/";
            $imageFileType = strtolower(pathinfo($_FILES["vehicle_picture"]['name'], PATHINFO_EXTENSION));
            $target_file = $target_dir . $_POST['create_vehicle_name'] . '.' . $imageFileType;
            if (move_uploaded_file($_FILES["vehicle_picture"]["tmp_name"], $target_file)) {
                $new_vehicle = new Vehicles($_POST['create_vehicle_name']);
                $new_vehicle->createVehicule($_POST['create_vehicle_price'],
                    $_POST['create_vehicle_category'], $target_file);

                $name = $new_vehicle->name;
            }
        }
        ?>
        <div class="row">
            <div class="col-xl-10 col-md-10 mb-12">
                <form method="post" action="index.php?page=admin" enctype="multipart/form-data">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="create_vehicles">Nom du véhicule</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Exemple : BF400"
                               required name="create_vehicle_name">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="create_vehicle_price">Prix</span>
                        </div>
                        <input type="number" class="form-control" name="create_vehicle_price"
                               required min="1" max="999999" value="1">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="create_vehicle_category">Catégorie</span>
                        </div>
                        <select class="custom-select" id="input_register_profession"
                                required name="create_vehicle_category">
                            <option value="roadster">Roadster</option>
                            <option value="sportive">Sportive</option>
                            <option value="chopper">Chopper</option>
                            <option value="offroad">Off-Road</option>
                            <option value="withoutlicense">Sans Permis</option>
                        </select>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="vehicle_picture">Photo</span>
                        </div>
                        <input type="file" class="form-control" accept=".png, .jpeg, .jpg"
                               required name="vehicle_picture">
                    </div>

                    <div style="text-align: center">
                        <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm"
                                name="create-vehicle-button">Valider
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>

<section class="about_part section-padding" id="about">
    <div class="container">
        <div class="row">
            <div class="section_tittle">
                <h2>Modification <span>de produit</span></h2>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="users" style="width='100%'; cellspacing='0'">
                    <thead> <!-- Apparaitra en haut -->
                    <tr>
                        <th class="red">N°</th>
                        <th class="red">Nom</th>
                        <th class="red">Prix</th>
                        <th class="red">Catégorie</th>
                        <th class="red">Modifier</th>
                        <th class="red">Supprimer</th>
                    </tr>
                    </thead>
                    <?php
                    $BDD = new BDD();
                    $dbh = $BDD->getConnection();
                    $stmt = $dbh->query('SELECT * FROM vehicles ');

                    echo '<tbody>';
                    foreach ($stmt as $row) {
                        echo '<tr><form method="post" action="index.php?page=update_vehicule" > <!-- Contenu, géré via bdd -->
                          <td>' . $row['id'] . '</td>
                          <td>' . $row['name'] . '</td>
                          <td>' . $row['price'] . '</td>
                          <td>' . $row['category'] . '</td>
                          <input type="hidden" value="' . $row['id'] . '" name="currentId">
                          <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm modifUser" name="modifVehicule"></button>
                           </td>
                           <td style="text-align: center">
                            <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm modifUser" name="deleteVehicule"></button>
                           </td>
                        </form></tr>';

                    }
                    echo '</tbody>';
                    ?>
                </table>
            </div>
        </div>
    </div>
</section>
