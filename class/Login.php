<?php


class Login {
  public $username;
  public $pswd;

  public function __construct($username, $pswd) {
    $this->username = $username;
    $this->pswd = $pswd;
  }


  public function connexion() {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $stmt = $dbh->prepare('SELECT * FROM admin');
    $stmt->execute();
    $resultat = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($resultat as $row) {
      if ($row['username'] == $this->username) {
        if ($row['pswd'] == $this->pswd) {
          $_SESSION["id"] = (int)$row["id"];
          $_SESSION["username"] = $this->username;
          $_SESSION["pswd"] = $this->pswd;

          echo '<script> document.location.replace("index.php?page=admin");</script>';

        } else {
          echo '<div class="alert alert-danger float" role="alert" style="width: 50%; margin: auto; margin-top: 1%">' .
              "La combinaison n'est pas valide, vérifiez votre mot de passe" .
              '</div>';
        }
      }
    }

  }
}
