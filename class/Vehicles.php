<?php


class Vehicles {
  public $name;

  public function __construct($name) {
    $this->name = $name;
  }

  public function createVehicule($price, $cat, $picture) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $test = $dbh->prepare('SELECT name FROM vehicles WHERE name = :name ');
    $test->execute(array(':name' => $this->name));
    $nbr = $test->rowCount();

    if ($nbr != 0) { // Si le nom est deja utilisé, le compteur va passé a 1
      echo '<div class="alert alert-danger" role="alert">' .
          "Un véhicule existe dèja à ce nom." .
          '</div>';
    } else {
      $BDD = new BDD();
      $dbh = $BDD->getConnection();
      $sth = $dbh->prepare('INSERT INTO vehicles(name, price, category, src) 
            VALUES (:name , :price, :cat, :src)');
      $sth->bindParam(':name', $this->name);
      $sth->bindParam(':price', $price);
      $sth->bindParam(':cat', $cat);
      $sth->bindParam(':src', $picture);
      $sth->execute();

      echo '<div class="alert alert-success float" role="alert">' .
          "Le véhicule a bien était créé." .
          '</div>';
    }
  }

  public function updateVehiculeWithImage($id, $price, $cat, $src) {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $test = $dbh->prepare('UPDATE vehicles SET name = :name, price = :price, category = :cat,
        src = :src WHERE id = :id');
        $test->execute(array(':name' => $this->name, ':price' => $price, ':cat' => $cat,
            ':src' => $src, ':id' => $id));

        echo '<div class="alert alert-success float" role="alert">' .
            "Le véhicule a bien était mis a jour." .
            '</div>';

  }

  public function updateVehicule($id, $price, $cat ) {
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $test = $dbh->prepare('UPDATE vehicles SET name = :name, price = :price, category = :cat
            WHERE id = :id');
        $test->execute(array(':name' => $this->name, ':price' => $price, ':cat' => $cat, ':id' => $id));

        echo '<div class="alert alert-success float" role="alert">' .
            "Le véhicule a bien était mis a jour." .
            '</div>';

    }
}
