$(document).ready(function(){
    
    (function($) {
        "use strict";

    
    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                lastname: {
                    required: true,
                    minlength: 2
                },
                subject: {
                    required: true,
                    minlength: 4
                },
                number: {
                    required: true,
                    minlength: 7,
                    maxlength: 7
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 20
                }
            },
            messages: {
                name: {
                    required: "Allez, tu as un nom, n'est ce pas ?",
                    minlength: "Votre nom doit comporter plus de 2 characters"
                },
                lastname: {
                    required: "tu as oublié comment tu t'appelle ?",
                    minlength: "Votre prénom doit comporter plus de 2 characters"
                },
                subject: {
                    required: "Allez, tu sais pourquoi tu nous contact, n'est ce pas ?",
                    minlength: "Votre sujet doit comporter plus de 4 characters"
                },
                number: {
                    required: "Allez, tu as un numéro de téléphone ( GTA ), n'est ce pas ?",
                    minlength: "Votre numéro de téléphone doit avoir 7 characters (ex: 5550000)",
                    maxlength: "Votre numéro de téléphone doit avoir 7 characters (ex: 5550000)"
                },
                email: {
                    required: "pas d'email, pas de message ..."
                },
                message: {
                    required: "Hum...ouais, tu doit ecrire quelque chose pour envoyer se formulaire.",
                    minlength: "C'est tout? vraiment? Un peu plus de précision ..."
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    data: $(form).serialize(),
                    url:"contact_process.php",
                    success: function() {
                        $('#contactForm :input').attr('disabled', 'disabled');
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor','default');
                            $('#success').fadeIn()
                            $('.modal').modal('hide');
		                	$('#success').modal('show');
                        })
                        window.location.replace("https://www.thelegendsrp.fr/sanders_motorcycle/index.php?page=contact");
                    },
                    error: function() {
                        $('#contactForm').fadeTo( "slow", 1, function() {
                            $('#error').fadeIn()
                            $('.modal').modal('hide');
		                	$('#error').modal('show');
                        })
                    }
                })
            }
        })
    })
        
 })(jQuery)
})
