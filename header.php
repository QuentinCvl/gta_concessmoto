<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Sanders MotorCycle</title>
  <!--<link rel="icon" href="img/favicon.png">-->
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <!-- animate CSS -->
  <link rel="stylesheet" href="css/animate.css">
  <!-- owl carousel CSS -->
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <!-- themify CSS -->
  <link rel="stylesheet" href="css/themify-icons.css">
  <!-- flaticon CSS -->
  <link rel="stylesheet" href="css/flaticon.css">
  <!-- magnific-popup CSS -->
  <link rel="stylesheet" href="css/magnific-popup.css">
  <!-- font awesome CSS -->
  <link rel="stylesheet" href="fontawesome/css/all.min.css">
  <!-- 404 page style -->
  <link rel="stylesheet" href="css/error404.css">
  <!-- style CSS -->
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
<header class="main_menu home_menu">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand" href="index.php" style="width: 20%">
            <img src="img/logo.png" alt="logo" style="width: 100%">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                  aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse main-menu-item" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="index.php?page=homepage#">Accueil</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="index.php?page=homepage#about">A Propos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="index.php?page=catalogue">Catalogues</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Extra
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="index.php?page=event">Evénements</a>
                  <a class="dropdown-item" href="index.php?page=news">Nouveautés</a>
                  <a class="dropdown-item" href="index.php?page=destockage">Destockage</a>
                  <a class="dropdown-item" href="index.php?page=partner">Nos Partenaires</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="index.php?page=contact">Nous Contacter</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="index.php?page=admin">Administration</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>
  <?php
  if(isset($_GET['page']) && $_GET['page'] == 'catalogue'){
    echo '<div class="container">
    <div class="navbar-collapse second-menu-item" id="navbarNav">
      <a class="nav-link" href="#withoutlicense">Sans Permis</a>
      <a class="nav-link" href="#roadster">Roadster</a>
      <a class="nav-link" href="#chopper">Chopper</a>
      <a class="nav-link" href="#offroad">Off-Road</a>
      <a class="nav-link" href="#sportive">Sportive</a>


    </div>
  </div>';
  }

  ?>
</header>
