<footer class="footer_part">
  <div class="container">
    <div class="single_footer_part" style="text-align: center">
      <p>Ce Site n'est pas un site E-commerce, il fait office uniquement de catalogue en ligne, est n'est destiné
        qu'à GTA RP</p>
    </div>
    <hr>
    <div class="row">
      <div class="col-lg-12">
        <div class="copyright_text text-center">
          <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script>
            All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a
                  href="https://colorlib.com" target="_blank">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- jquery plugins here-->
<script src="js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- magnific js -->
<script src="js/jquery.magnific-popup.min.js"></script>
<!-- carousel js -->
<script src="js/owl.carousel.min.js"></script>
<!-- easing js -->
<script src="js/jquery.easing.min.js"></script>
<!--masonry js-->
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/masonry.pkgd.js"></script>
<!--form validation js-->
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/contact.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/mail-script.js"></script>
<!-- counter js -->
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
</body>

</html>
