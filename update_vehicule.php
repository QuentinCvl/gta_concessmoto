<?php
if (isset($_POST['modifVehicule'])) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $req = $dbh->prepare('SELECT * FROM vehicles WHERE id = ?');
    $req->execute(array($_POST['currentId']));
    $req = $req->fetch(PDO::FETCH_ASSOC);
} else {
    echo '<script> document.location.replace("index.php?page=admin");</script>';
}
?>
<section class="breadcrumb blog_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner">
                    <div class="breadcrumb_iner_item">
                        <h2> Modification de : <?php if (isset($req['name'])) {
                                echo $req['name'];
                            } else {
                                echo "error";
                            } ?> </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Begin Page Content -->
<div class="container-fluid" style="position: relative">
    <!-- Page Heading -->
    <div class="info-vehicle-container">
        <p><strong>Nom du véhicule : </strong><?php if (isset($req['name'])) {
                echo $req['name'];
            } else {
                echo "error";
            } ?></p>
        <p><strong>N° : </strong><?php if (isset($req['id'])) {
                echo $req['id'];
            } else {
                echo "error";
            } ?></p>
        <p><strong>Prix : </strong><?php if (isset($req['price'])) {
                echo $req['price'];
            } else {
                echo "error";
            } ?></p>
        <div class="vehicule-img">
            <img src="<?php if (isset($req['src'])) {
                echo $req['src'];
            } else {
                echo "error";
            } ?>">
        </div>
    </div>


    <hr class="sidebar-divider my-3" style="width: 50%">

    <div class="row">
        <div class="col-xl-12 col-md-12 mb-12 about_part">
            <div class="section_tittle">
                <h2>Modifier <span>le véhicule</span></h2>
            </div>
            <form method="post" action="index.php?page=admin">
                <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="update_vehicle_name">Nom</span>
                    </div>
                    <input type="text" class="form-control" aria-label="Name" required
                           value="<?php if (isset($req['name'])) {
                               echo $req['name'];
                           } else {
                               echo "error";
                           } ?>"
                           aria-describedby="update_vehicle_name" name="update_vehicle_name">
                </div>

                <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="update_vehicle_price">Prix</span>
                    </div>
                    <input type="number" class="form-control" aria-label="Price" required
                           value="<?php if (isset($req['price'])) {
                               echo $req['price'];
                           } else {
                               echo "error";
                           } ?>"
                           aria-describedby="update_vehicle_price" name="update_vehicle_price">
                </div>

                <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="update_vehicle_cat">Catégorie</span>
                    </div>
                    <select class="custom-select" id="update_vehicle_cat"
                            required name="update_vehicle_cat">
                        <option value="roadster" <?php if ($req['category'] == 'roadster') echo 'selected' ?>>Roadster
                        </option>
                        <option value="sportive" <?php if ($req['category'] == 'sportive') echo 'selected' ?>>Sportive
                        </option>
                        <option value="chopper" <?php if ($req['category'] == 'chopper') echo 'selected' ?>>Chopper
                        </option>
                        <option value="offroad" <?php if ($req['category'] == 'offroad') echo 'selected' ?>>Off-Road
                        </option>
                        <option value="withoutlicense" <?php if ($req['category'] == 'withoutlicense') echo 'selected' ?>>
                            Sans Permis
                        </option>
                    </select>

                    <input type="hidden" value="<?php echo $req['id'] ?>" name="update_vehicle_id">
                    <input type="hidden" value="<?php echo $req['src'] ?>" name="curent_vehicle_src">
                </div>

                <div class="input-group mb-3 col-xl-10 col-md-10 mb-10">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="update_vehicle_src">Photo</span>
                    </div>
                    <input type="file" class="form-control" accept=".png, .jpeg, .jpg"
                           name="update_vehicle_src">
                </div>
                <div class="col-xl-10 col-md-10 mb-10">
                    <span style="color: #ff3334">/!\ Ne rien mettre si vous ne voulez pas remplacer l'image !</span>
                </div>

                <div style="text-align: center; margin-top: 5px" class="mb-3 col-xl-10 col-md-10 mb-10">
                    <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"
                            name="update_vehicle_button">
                        Valider
                    </button>
                </div>
            </form>
        </div>
    </div>

</div>

<script>

    $('#showUpdateCriminalRecord').on('click', function () {
        var show = document.getElementById('updateCriminalRecord');
        show.style.display = "block";
        var hide = document.getElementById('showUpdateCriminalRecord');
        hide.className = "d-none";
    })
</script>
