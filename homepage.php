<!--::banner part start::-->
<section class="banner_part">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 offset-lg-1 col-sm-8 offset-sm-2">
        <div class="banner_text aling-items-center">
          <div class="banner_text_iner">
            <h5>LSSM</h5>
            <h2>Los Santos <br>
              Sanders Motorcycles</h2>
            <p>Bienvenue sur le Site Web de la concession de moto de Los Santos.
              Vous trouverez ici toutes les informations importantes, catalogue, etc ... </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--::banner part end::-->

<!--::team part end::-->
<section class="about_part section-padding" id="about">
  <div class="container">
    <div class="row">
      <div class="section_tittle">
        <h2>A <span>Propos</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6" style="align-self: center;">
        <div class="about_img">
          <img src="img/motos.jpeg" alt="">
        </div>
      </div>
      <div class="offset-lg-1 col-lg-5 col-sm-8 col-md-6">
        <div class="about_text">
          <h2>LSSM à <span>votre service</span></h2>
          <p>Sanders Motorcycle s'efforce de vous fournir au quotidien la meilleur expérience
            de conduite à bord de nos véhicules de prestige.</p>
          <a href="#ourproject" class="btn_1">En savoir plus</a>
          <div class="about_part_counter">
            <div class="single_counter">
              <span class="counter">50</span>
              <p>Véhicules</p>
            </div>
            <div class="single_counter">
              <span class="counter">300</span>
              <p>clients</p>
            </div>
            <div class="single_counter">
              <span class="counter">14</span>
              <p>ans d'expertise</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--::team part end::-->

<!--::project part start::-->
<section class="portfolio_area pt_30 padding_bottom" id="ourproject">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section_tittle">
          <h2><span>Vos</span> projet</h2>
        </div>
        <div class="portfolio-filter">
          <h2>Un panel de selection <br>
            adaptés à <span>vos besoin.</span></h2>
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li>
              <a class="active" id="Architecture-tab" data-toggle="tab" href="#Architecture" role="tab"
                 aria-controls="Architecture" aria-selected="true">
                Sportive
              </a>
            </li>
            <li>
              <a id="Interior-tab" data-toggle="tab" href="#Interior" role="tab" aria-controls="Interior"
                 aria-selected="false">
                Choppers
              </a>
            </li>
            <li>
              <a id="Exterior-tab" data-toggle="tab" href="#Exterior" role="tab" aria-controls="Exterior"
                 aria-selected="false">
                Scooter / Vélos
              </a>
            </li>
            <li>
              <a id="Landing-tab" data-toggle="tab" href="#Landing" role="tab" aria-controls="Landing"
                 aria-selected="false">
                Tout Terrain
              </a>
            </li>
          </ul>
        </div>
        <div class="portfolio_item tab-content" id="myTabContent">
          <div class="row align-items-center justify-content-between tab-pane fade show active"
               id="Architecture" role="tabpanel" aria-labelledby="Architecture-tab">
            <div class="col-lg-6 col-sm-12 col-md-6">
              <div class="portfolio_box">
                <a href="img/project/pegassi-bati.jpg" class="img-gal">
                  <div class="single_portfolio">
                    <img class="img-fluid w-100" src="img/project/pegassi-bati.jpg" alt="">
                  </div>
                </a>
                <div class="short_info">
                  <p>Sportive</p>
                  <h4><a href="#">Pegassi Bati 800 <br>
                    Moto sportive</a></h4>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-6">
              <div class="row">
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/shitzu-vader.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/shitzu-vader.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>Sportive</p>
                      <h4><a href="#">Shitzu Vader <br>
                        Moto Sportive</a></h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/pegassi-ruffian.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/pegassi-ruffian.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>Exclusive Project</p>
                      <h4><a href="#">Pegassi Ruffian <br>
                        Moto Sportive</a></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row align-items-center justify-content-between tab-pane fade" id="Interior" role="tabpanel"
               aria-labelledby="Interior-tab">
            <div class="col-lg-6 col-sm-12 col-md-6">
              <div class="portfolio_box">
                <a href="img/project/hexer.jpg" class="img-gal">
                  <div class="single_portfolio">
                    <img class="img-fluid w-100" src="img/project/hexer.jpg" alt="">
                  </div>
                </a>
                <div class="short_info">
                  <p>Chopper</p>
                  <h4><a href="#">Hexer <br>
                    Moto Chopper</a></h4>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-6">
              <div class="row">
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/cliffhanger.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/cliffhanger.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>Chopper</p>
                      <h4><a href="#">Cliffhanger <br>
                        Moto Chopper</a></h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/sanctus.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/sanctus.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>new Project</p>
                      <h4><a href="#">Sanctus <br>
                        Moto Chopper</a></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row align-items-center justify-content-between tab-pane fade" id="Exterior" role="tabpanel"
               aria-labelledby="Exterior-tab">
            <div class="col-lg-6 col-sm-12 col-md-6">
              <div class="portfolio_box">
                <a href="img/project/faggio.jpg" class="img-gal">
                  <div class="single_portfolio">
                    <img class="img-fluid w-100" src="img/project/faggio.jpg" alt="">
                  </div>
                </a>
                <div class="short_info">
                  <p>Scooter</p>
                  <h4><a href="#">Faggio <br>
                    Scooter Sans Permis</a></h4>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-6">
              <div class="row">
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/bmx.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/bmx.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>Scooter</p>
                      <h4><a href="#">BMX <br>
                        Vélo Tout Terrain</a></h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/endurex-race-bike.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/endurex-race-bike.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>Vélo</p>
                      <h4><a href="#">Endurex Race <br>
                        Vélo de course</a></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row align-items-center justify-content-between tab-pane fade" id="Landing" role="tabpanel"
               aria-labelledby="Landing-tab">
            <div class="col-lg-6 col-sm-12 col-md-6">
              <div class="portfolio_box">
                <a href="img/project/bf400.jpg" class="img-gal">
                  <div class="single_portfolio">
                    <img class="img-fluid w-100" src="img/project/bf400.jpg" alt="">
                  </div>
                </a>
                <div class="short_info">
                  <p>Moto Cross</p>
                  <h4><a href="#">Nagasaki BF400 <br>
                    Moto Tout Terrain </a></h4>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-6">
              <div class="row">
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/nagasaki-street-blazer.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/nagasaki-street-blazer.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>Quad</p>
                      <h4><a href="#">Street Blazer <br>
                        Quad Tout Terrain</a></h4>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                  <div class="portfolio_box">
                    <a href="img/project/maibatsu-sanchez.jpg" class="img-gal">
                      <div class="single_portfolio">
                        <img class="img-fluid w-100" src="img/project/maibatsu-sanchez.jpg" alt="">
                      </div>
                    </a>
                    <div class="short_info">
                      <p>Moto Cross</p>
                      <h4><a href="#">Maibatsu Sanchez <br>
                        Moto Tout Terrain</a></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--::project part end::-->
