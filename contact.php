<!--::breadcrumb part start::-->
<section class="breadcrumb blog_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner">
                    <div class="breadcrumb_iner_item">
                        <h2>Contact</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--::breadcrumb part start::-->

<!-- ================ contact section start ================= -->
<section class="contact-section area-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">Entrer en contact</h2>
            </div>
            <div class="col-lg-8">
                <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm"
                      novalidate="novalidate">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                <textarea class="form-control w-100 placeholder hide-on-focus" name="message" id="message" cols="30"
                          rows="9" placeholder="Entrer votre Message"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control placeholder hide-on-focus" name="name" id="name" type="text"
                                       placeholder="Nom">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control placeholder hide-on-focus" name="lastname" id="lastname"
                                       type="text"
                                       placeholder="Prenom">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control placeholder hide-on-focus" name="number" id="number" type="number"
                                       placeholder="Téléphone">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control placeholder hide-on-focus" name="email" id="email"
                                       type="email"
                                       placeholder="Email">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <input class="form-control placeholder hide-on-focus" name="subject" id="subject"
                                       type="text"
                                       placeholder="Sujet">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="button button-contactForm">Envoyer</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-4">
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-home"></i></span>
                    <div class="media-body">
                        <h3><a href="">Sanders Motorcycle, South Los Santos.</a></h3>
                        <p>237 Adam's Apple Boulevard</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                    <div class="media-body">
                        <h3><a href="tel:454545654">555 0000</a></h3>
                        <p>Lundi au Vendredi, de 9h à 18h</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-email"></i></span>
                    <div class="media-body">
                        <h3><a href="mailto:support@colorlib.com">contact.sandersmotorcycle@thelegendsrp.fr</a></h3>
                        <p>Envoyez-nous votre demande à tout moment !</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->
